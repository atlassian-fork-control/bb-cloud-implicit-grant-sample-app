
Bitbucket Cloud Implicit Grant Sample App
---

## Introduction
This app aims to demonstrate the Implicit Grant Type Authorization flow, what is needed to generate an access_token and how to use that to invoke Bitbucket REST APIs, and lastly a reference app where the developer can copy paste the code and try it in their own projects

## What is an Implicit Grant?
The Authorization Code grant type is the most common type of Authorization and used by web and mobile apps. It differs from most of the other grant types by first requiring the app launch a browser to begin the flow. This also represents the full blown 3-LO flow.

When a user authorizes an app to access his/her information, user is redirected back to the app with a temporary code that the app can exchange for an access token, which can be used to access the user's data. This is probably the most secure way of getting the access token as the process is not visible to the user and is done on the back-end.


### When to use it
- When you want to invoke APIs on the user's behalf

#### Practical Usage/Real life use cases

### When not to use this type of Authorization

### Scope and Limitations of the Authorization
=======
In the implicit flow, instead of issuing the client an authorization code, the client is issued an access token directly (as the result of the resource owner authorization).  The grant type is implicit as no intermediate credentials (such as an authorization code) are issued (and later used to obtain an access token).

### Difference with other grants
- Don't need user initiation
-  Authorization server does not authenticate the client
- access_token is exposed as a query parameter after the # in the URL
- Security issues due to [Access Token](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-10.3) and [Misuse of Access Token to Impersonate Resource Owner in Implicit Flow](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-10.16)

### Authorization Flow Chart
![Alt text](public/images/authorization_flow.png?raw=true "Bitbucket Cloud Implicit Grant")

### Things to consider
*What is OAuth?*

OAuth is a mechanism for authorizing an entity and granting them access over which the user is capable of doing in an application

*What it is not?*

OAuth is not a way to authenticate but rather to authorize an entity for access

*Definition of terms*

| Term          									| Description   |
|---------------------------------------------------|-------------|
|Basic Auth    										|Is an authentication mechanism. Is straightforward but unsecure. It is the combination of your username and password that you would use to access an API endpoint.|
|client_id     										|Key generated from Bitbucket Settings → Add Consumer   |
|secret        										|Secret generated from Bitbucket Settings → Add Consumer|
|https://bitbucket.org/site/oauth2/authorize        |URL that you use to authorize an entity|
|https://bitbucket.org/site/oauth2/access_token		|URL that generates an access token for an entity|
|access_token										|Defines the permission of the selected user, ergo represents the authorization of a specific application to access specific parts of a user's data. Tokens that represents the authorization of an entity to access specific parts of a user's data in an application. It is the token generated or returned by https://bitbucket.org/site/oauth2/access_token|

*Participants*

| Role          									| Description   |
|---------------------------------------------------|-------------|
|Resource Owner/End-user    						|An entity capable of granting access to a protected resource. When the resource owner is a person, it is referred to as an end- user.|
|Client     										|An application making protected resource requests on behalf of the resource owner and with its authorization.|
|Resource Server        							|The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens.|
|Authorization Server						       	|The server issuing access tokens to the client after successfully authenticating the resource owner and obtaining authorization.|

## Prerequisite
1. NodeJs

## Running the app
1. You are free to use the consumer key and secret configured in this project or generate your own. If you are [creating your own oauth consumer](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html#OAuthonBitbucketCloud-Createaconsumer), please make sure to the callback URL to `http://localhost:3000/oauth-callback` as `/oauth-callback` processes the response from Bitbucket's Authorization server, just replace the consumerKey and consumerSecret in config.json and you're good to go.
2. In the project's root directory, run `npm install`. This should download all the dependencies for you needed by the app
3. Once download is complete, run `npm start`
4. Open your browser and launch: http://localhost:3000/ , this should display an intuitive page on how to go through the authorization flow


## Navigating the app

| Endpoint 			| Description |
|-------------------|-------------|
| /					|Redirects the user to the landing page of the sample app |
| /healthcheck		|Test whether your app is alive							|
| /oauth-callback	|This is the callback URL that the authorization server recognizes that would receive the temporary code (if authorization code grant is used) and access_tokens for others|

## Support

If you have any questions about this app or simply want to give us feedback please do so in the [Bitbucket Cloud category](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud) on the Developer Community.

## Related Documentation
- [Bitbucket OAuth 2.0](https://developer.atlassian.com/cloud/bitbucket/oauth-2/)
- [IETF - Implicit Grant](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.2)
- [OAuth Participants](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.1)
